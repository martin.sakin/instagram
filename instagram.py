#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Author: Martin Sakin, martin.sakin <at> gmail.com
# @Date: 2020-10-31
# LastUpdate: 2021-05-11
# Download images/videos from instagram.com
# Just run script without arguments, it will print help
# TODO: video cannot download, directory

import datetime
import json
import os
import requests
import sys

LOG = True

class Info():
	def __init__(self):
		self.username = ""
		self.shortcode = ""
		self.taken_at_timestamp = None
		self.url = ""
		self.ides = []
		self.urls = []
		self.is_video = False
		self.error_msg = ""

	def addSource(self, idcode, url):
		self.ides.append(idcode)
		self.urls.append(url)

	def downloadList(self):
		if self.ides:
			theList = []
			for i in range(len(self.ides)):
				theList.append( (self.createFilename(self.ides[i]), self.urls[i]) )
			return theList
		else:
			return [ (self.output_filename(), self.url) ]

	def createFilename(self, idecode):
		filetype = self.url.split('?')[0].split('.')[-1]
		return self.username + "_" + self.time() + "_" + self.shortcode + "_" + idecode + "." + filetype

	def time(self):
		taken_at = datetime.datetime.fromtimestamp(self.taken_at_timestamp)
		return str(taken_at).replace(' ','_').replace(':','-')

	def output_filename(self):
		filetype = self.url.split('?')[0].split('.')[-1]
		return self.username + "_" + self.time() + "_" + self.shortcode + "." + filetype

	def is_valid(self):
		return self.username and self.shortcode and self.taken_at_timestamp and self.url

	def page(self):
		return ("https://www.instagram.com/p/" + self.shortcode + "/") if self.shortcode else self.error_msg

	def print(self):
		print('username', ':', self.username)
		print('shortcode', ':', self.shortcode)
		print('taken_at_timestamp', ':', self.taken_at_timestamp)
		print('url', ':', self.url)
		print('is_video', ':', self.is_video)
		print('error_msg', ':', self.error_msg)


def eprint(*args, **kwargs):
	print(" ".join(map(str,args)), **kwargs)


def get_JSON(address):
	if not address:
		return None
	address = address.replace(chr(65279),'')	# zero-width no-break space

	headers = {
			'User-Agent': 'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0',
	}
	r = requests.get(address, headers=headers)
	content = r.text.replace('>','>\n')

	for line in content.split('\n'):
		if line.find('window._sharedData =') > -1:
			line = line.replace('window._sharedData = ','').replace(';</script>','')
			return json.loads(line)

	print("! no JSON")
	return None


def extract_info(json_data):
	info = Info()
	if not json_data:
		return info
	if not 'entry_data' in json_data or not 'PostPage' in json_data['entry_data']:
		info.error_msg = "Page isn't available"
		return info

	if json_data['entry_data']['PostPage'][0]['graphql']['shortcode_media']:
		smedia = json_data['entry_data']['PostPage'][0]['graphql']['shortcode_media']
		try:
			info.username = smedia['owner']['username']
			info.taken_at_timestamp = smedia['taken_at_timestamp']
			info.shortcode = smedia['shortcode']
			info.is_video = smedia['is_video']
			if info.is_video:
				info.url = smedia['video_url']
			else:
				info.url = smedia['display_url']

			if 'edge_sidecar_to_children' in smedia:
				for edge in smedia['edge_sidecar_to_children']['edges']:
					info.addSource(edge['node']['shortcode'], edge['node']['display_url'])

		except Exception as e:
			eprint("extract_info():", e)
			pass
	else:
		info.error_msg = "Content isn't available"
		return info

	return info


def extract_user_info(json_data):
	info_list = []
	if json_data['entry_data'] == {'HttpErrorPage': [{}]}:
		eprint("Page isn't available")
		return []
	try:
		username = json_data['entry_data']['ProfilePage'][0]['graphql']['user']['username']
		edges = json_data['entry_data']['ProfilePage'][0]['graphql']['user']['edge_owner_to_timeline_media']['edges']

		for node in edges:
			info = Info()
			info.username = username
			info.taken_at_timestamp = node['node']['taken_at_timestamp']
			info.shortcode = node['node']['shortcode']
			info.is_video = node['node']['is_video']
			if info.is_video:
				info.url = extract_info(get_JSON("https://www.instagram.com/p/" + info.shortcode + "/")).url
				pass
			else:
				info.url = node['node']['display_url']
			info_list.append(info)
	except Exception as e:
		print("extract_user_info():", e)
		pass
	return info_list


def download(info):
	if not info.is_valid():
		eprint("! Cannot download: ", info.page())
		return
	for filename, link in info.downloadList():
		with open(filename, 'wb') as f:
			f.write(requests.get(link).content)
		if LOG: print(filename)


def download_from_url(url):
	url = url.split('?')[0].strip()
	download(extract_info(get_JSON(url)))


def urls_from_file(filename):
	if not os.path.isfile(filename):
		eprint("File '" + filename + "' not found")
		exit(1)

	try:
		with open(filename, 'r') as f:
			for url in f:
				url = url.strip()
				url = url.split('?')[0].strip()
				if not url or url.startswith('#') or url.startswith(';'):
					continue
				download(extract_info(get_JSON(url)))

	except FileNotFoundError:
		eprint("Some problem with file '" + filename + "'")
		exit(1)


def download_user(username):
	info_list = extract_user_info(get_JSON('https://www.instagram.com/' + username + '/'))
	for info in info_list:
		download(info)


def help():
	text = ("=== Instagram image downloader ===\n" +
			"HELP:\n" +
			"- Download one image from link:\n  " +
			sys.argv[0] + ' <https://www.instagram.com/p/XXXXXXXXXXX/>\n' +
			"- Download more images from file with links:\n  " +
			sys.argv[0] + ' <file_with_urls.x>\n' +
			"- Download last 12 images from a user:\n  " +
			sys.argv[0] + ' --user <insta_name>')
	print(text)
	exit(0)


if __name__ == '__main__':
	if len(sys.argv) == 2:
		if sys.argv[1].find('instagram.com/p/') > -1:
			download_from_url(sys.argv[1])
		elif sys.argv[1] in ['-h', '--help']:
			help()
		else:
			urls_from_file(sys.argv[1])
	elif len(sys.argv) == 3 and sys.argv[1] in ['-u','--user']:
		download_user(sys.argv[2])
	else:
		help()
