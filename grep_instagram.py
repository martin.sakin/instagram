#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Author: Martin Sakin, martin.sakin <at> gmail.com
# @Date: 2021-05-15
# Parse images/videos link from instagram.com source page
# MANUAL:
#	In Chrome open view-source (Ctrl+U), select all (Ctrl+A), copy (Ctrl+C)
#	and paste into source.html (Ctrl+V), save (Ctrl+S) and run
#	./grep_instagram source.html

import sys
import json
import re
import datetime
import requests

LOG = True

class Info():
	def __init__(self):
		self.username = ""
		self.shortcode = ""
		self.taken_at_timestamp = None
		self.url = ""
		self.ides = []
		self.urls = []
		self.is_video = False
		self.error_msg = ""

	def __str__(self):
		return self.__inside()

	def addSource(self, idcode, url):
		self.ides.append(idcode)
		self.urls.append(url)

	def downloadList(self):
		if self.ides:
			theList = []
			for i in range(len(self.ides)):
				theList.append( (self.createFilename(self.ides[i], self.urls[i]), self.urls[i]) )
			return theList
		else:
			return [ (self.output_filename(), self.url) ]

	def createFilename(self, idecode, url=""):
		filetype = self.url.split('?')[0].split('.')[-1]
		if not filetype:
			filetype = url.split('?')[0].split('.')[-1]
		return self.username + "_" + self.time() + "_" + self.shortcode + "_" + idecode + "." + filetype

	def time(self):
		taken_at = datetime.datetime.fromtimestamp(self.taken_at_timestamp)
		return str(taken_at).replace(' ','_').replace(':','-')

	def output_filename(self):
		filetype = self.url.split('?')[0].split('.')[-1]
		return self.username + "_" + self.time() + "_" + self.shortcode + "." + filetype

	def is_valid(self):
		return self.username and self.shortcode and self.taken_at_timestamp and (self.url or self.urls)

	def page(self):
		return ("https://www.instagram.com/p/" + self.shortcode + "/") if self.shortcode else self.error_msg

	def __inside(self):
		txt = (
			'username: ' + self.username + '\n' +
			'shortcode: ' + self.shortcode + '\n' +
			'taken_at_timestamp: ' + str(self.taken_at_timestamp) + '\n' +
			'url: ' + self.url + '\n' +
			'is_video: ' + str(self.is_video) + '\n' +
			'error_msg: ' + self.error_msg
		)
		return txt


def extract_info(json_data):
	info = Info()
	if not json_data:
		return info

	if json_data['items'][0]:
		smedia = json_data['items'][0]
		try:
			info.username = smedia['user']['username']
			info.taken_at_timestamp = smedia['taken_at']
			info.shortcode = smedia['code']
			info.is_video = 'video_versions' in smedia

			info.is_multiple = 'carousel_media' in smedia

			if info.is_multiple:
				for edge in smedia['carousel_media']:
					org_width = edge['original_width']
					org_height = edge['original_height']
					for candidate in edge['image_versions2']['candidates']:
						if candidate['width'] == org_width and candidate['height'] == org_height:
							info.addSource(edge['id'], candidate['url'])
			else:
				org_width = smedia['original_width']
				org_height = smedia['original_height']

				if info.is_video:
					info.url = smedia['video_versions'][0]['url']
				else:
					for candidate in smedia['image_versions2']['candidates']:
						if candidate['width'] == org_width and candidate['height'] == org_height:
							info.url = candidate['url']
							break

		except Exception as e:
			print("extract_info():", e)
			pass
	else:
		info.error_msg = "Content isn't available"
		return info

	return info


def download(info):
	if not info.is_valid():
		print("! Cannot download: ", info.page())
		return
	for filename, link in info.downloadList():
		with open(filename, 'wb') as f:
			f.write(requests.get(link).content)
		if LOG: print(filename)


def getJSON(content):
	content = content.replace('>','>\n')
	sharedData = ""
	additionalData = ""

	for line in content.split('\n'):
		if line.find("window.__additionalDataLoaded(") > -1:
			line = re.sub(r'window.__additionalDataLoaded.*,\{"items"', '{"items"', line)
			line = line.replace(");</script>", "")
			json_data = json.loads(line)
			return json_data

	return (sharedData, additionalData)


def main(filename):
	with open(filename, 'r') as file:
		content = file.read()
		additionalData = getJSON(content)
		info = extract_info(additionalData)
		download(info)


def help():
	print("Give me filename with 'view-source'!  e.g.:")
	print(" ", sys.argv[0], "source.html")


if __name__ == '__main__':
	if len(sys.argv) == 2:
		main(sys.argv[1])
	else:
		help()
