# Instagram image downloader

This script downloads public images on instagram from link (without login).
You do not need any special libraries, it uses the basic Python3 libraries.


Last update: 2021-05-11

### Get link of image

When you are on the main timeline `https://www.instagram.com/`
or open a image on some profile page,
try to find the time of the post somewhere down (e.g. 42 MINUTES AGO or 4 HOURS AGO or 2 DAYS AGO).

This time is a link.
Right-click on it and select "Copy link address"
or left-click on it (it opens the post) and copy the address from the address bar.
(e.g. `https://www.instagram.com/p/CNsAG6jhxbv/`)


## Download image

You have three options:
  - download one specific image
  - download images from file with links
  - download last 12 images from a specific user


**Print help**: You can print help just by running a script without any arguments:

	python3 ./instagram.py

**Download one specific image**: Give the obtained link to the image as a script argument:

	python3 ./instagram.py https://www.instagram.com/p/XXXXXXXXXXX/

**Download images from file with links**: Give a name of your file (any name) with a list of links:

	python3 ./instagram.py file_with_urls.x

In the file always have one link per line, lines starting with a symbols '#' or ';' (for comments) are ignored:

```
> cat my_links.txt
https://www.instagram.com/p/XXXXXXXXXXX/
https://www.instagram.com/p/YYYYYYYYYYY/
# some comment
https://www.instagram.com/p/ZZZZZZZZZZZ/
; another comment
```

**Download last 12 images from a specific user**: The first argument is the keyword *"user"*, the second argument is a *username*:

	python3 ./instagram.py --user someInstaName
	# or
	python3 ./instagram.py -u someInstaName

---


## Download image from html source code

If user's content is visible only with login.

In Chrome open view-source (Ctrl+U), select all (Ctrl+A), copy (Ctrl+C)
and paste into source.html (Ctrl+V), save (Ctrl+S) and run
`python3 ./grep_instagram source.html`
